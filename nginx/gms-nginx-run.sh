NGINX_IMAGE_NAME=gms/nginx:latest
GMS_NETWORK=gms-net
NGINX_APP_NAME=gms-nginx


docker rm -f ${NGINX_APP_NAME}

docker run -d \
    --net=${GMS_NETWORK} \
    -p 8443:443 \
    --name ${NGINX_APP_NAME} \
    ${NGINX_IMAGE_NAME}