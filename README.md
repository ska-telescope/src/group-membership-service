## Description
Group Membership service is an IVOA-compatible wrapper service which is used to query the group membership questions to INDIGO-IAM to know if any user is part of any set of groups or not.

## Setup of Indigo IAM and the Group Membership Service
1. Check the iam-login-service from https://github.com/indigo-iam/iam/tree/master/iam-login-service. Run db, Iam-login-service in docker and in same network.
   1. ``` docker network create iam ```
   2. ```
      docker run \
      --name db \
      --network iam \
      -e MYSQL_DATABASE=iam \
      -e MYSQL_USER=iam \
      -e MYSQL_ROOT_PASSWORD=pwd \
      -e MYSQL_PASSWORD=pwd \
      -d -p 3306:3306 mysql
      ```
   3. ```
      The Iam-login-service has already a keystore,jwks in the resource folder. 
      If we want to use the same then in env file we can specify the IAM_KEY_STORE_LOCATION path to the keystore path from resources folder.
      IAM_KEY_STORE_LOCATION=file:/indigo-iam/WEB-INF/classes/keystore.jwks
      then docker run command will be :
       docker run -d \
       --name iam-login-service \
       --net=iam -p 8080:8080 \
       --env-file=/iam/env \
       --restart unless-stopped \
       indigoiam/iam-login-service:v1.8.3
       
      if we have to provide keystore.jwks from outside then using volume mount we can provide the keystore.
      IAM_KEY_STORE_LOCATION=/keystore/keystore.jwks
      then docker run command will be :
       docker run -d \
       --name iam-login-service \
       --net=iam -p 8080:8080 \
       --env-file=\iam\env \
       -v /iam/keystore.jwks:/keystore/keystore.jwks:ro \
       --restart unless-stopped \
       indigoiam/iam-login-service:v1.8.3
      ```
   For reference please check https://indigo-iam.github.io/v/v1.8.3/docs/getting-started/docker/

2. Check that IAM is successfully connected with DB or not and register some dummy user and group, and add users to the group.
3. Run Iam-test-client in either docker or as jar as it is an independent service which will just connect to iam-login-service. Make sure everything is working, and we are able to log in with the dummy users in Iam-test-client successfully. You can find the iam-test-client in https://github.com/indigo-iam/iam/tree/master/iam-test-client
4. Create the `cadc-registry.properties` file to lookup OpenID Connect providers.
   ```properties
   ivo://ivoa.net/sso#OAuth = <provider URI or full URL for OAuth lookups> # (i.e. https://ska-iam.stfc.ac.uk)
   ivo://ivoa.net/sso#OpenID = <provider URI or full URL for OpenID Connect lookups> # (i.e. https://ska-iam.stfc.ac.uk)
   http://www.opencadc.org/std/posix#group-mapping-0.1 = <posix mapper service URI or full URL>
   http://www.opencadc.org/std/posix#user-mapping-0.1 = <posix mapper service URI or full URL>
   ca.nrc.cadc.reg.client.RegistryClient.baseURL = <base URL for the registry lookup> (i.e. https://spsrc27.iaa.csic.es/reg)
   ```
5. Run GMS in either docker or as JAR file and specify the environment variables
   1. Using docker:
      1. build docker image of GMS :
         ```docker build -t gms/gms:latest .```
      2. Run the gms container :
      ```
         docker run -d \
         --name gms \
         --net=iam -p 443:443 \
         -e JVM_OPTS="-Dca.nrc.cadc.auth.IdentityManager=org.opencadc.auth.StandardIdentityManager"
         -e IAM_HOST=https://ska-iam.stfc.ac.uk
         -e IAM_SCIM_CLIENT_ID="<oidc-client-id>"
         -e IAM_SCIM_CLIENT_SECRET="<oidc-client-secret>"
         -v $(pwd)/cadc-registry.properties:/root/config/cadc-registry.properties:ro
         --restart unless-stopped \
         gms/gms:latest
      ```
   2. Using JAR:
      1. Build the jar file using maven : ``` mvn clean verify -DskipTests ```
      2. Place the `cadc-registry.properties` into your `${HOME}/config` folder.
      1. Export the environment variables:
         1. ``` export IAM_HOST=https://ska-iam.stfc.ac.uk ```
         2. ``` export IAM_SCIM_CLIENT_ID=<oidc-client-id> ```
         3. ``` export IAM_SCIM_CLIENT_SECRET=<oidc-client-secret> ```
      1. Run the JAR: ``` java -Dca.nrc.cadc.auth.IdentityManager=org.opencadc.auth.StandardIdentityManager -jar target/group-management-service-0.0.1.jar ```

### Running integration tests (outdated - 2024.03.21)
1. Make sure iam-login-service and iam-test-client service are up and running.
2. Login with the admin user in iam-login-service
3. Add three groups(Developer,Admin,Viewer).
4. Add two user with username as userWithNoGroups and userWithGroups.
5. Add those three groups to userWithGroups.
6. Make sure you are able to log in with userWithNoGroups and userWithGroups user in iam-test-client.
7. Once you are logged in iam-test-client you will see authentication token in the dashboard.
8. copy the tokens for both of the user and paste it in the application.properties file in integration-test.
9. Update the GMS url in application.properties if needed.
10. Set enable as true in the tests in com.tw.gms.integration.GmsTest
11. Run ```mvn test -Dtest=com.tw.gms.integration.GmsTest -DfailIfNoTests=false -Dsurefire.suiteXmlFiles=src/test/resources/testng.xml```.