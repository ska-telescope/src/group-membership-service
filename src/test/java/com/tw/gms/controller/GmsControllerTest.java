package com.tw.gms.controller;

import com.tw.gms.model.GMSDomainModel;
import com.tw.gms.service.GmsService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(GmsController.class)
@WithMockUser
public class GmsControllerTest {
    @MockBean
    GmsService gmsService;
    @Autowired
    private MockMvc mockMvc;

    @Test
    void isAMemberWithToken() throws Exception {
        when(gmsService.isMember(Mockito.any(GMSDomainModel.class))).thenReturn("group1\ngroup2\n");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "token");
        mockMvc.perform(get("/gms/search")
                        .param("group", "group1")
                        .param("group", "group2")
                        .headers(headers))
                .andExpect(status().isOk())
                .andExpect(content().string("group1\ngroup2\n"));
    }

    @Test
    void isAMemberWithClientCert() throws Exception {
        when(gmsService.isMember(Mockito.any(GMSDomainModel.class))).thenReturn("group1\ngroup2\n");
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-SSL-Client-S-Dn", "CN=client-cert,O=E4R,L=Pune,ST=MH,C=IN");
        headers.add("X-SSL-Client-Cert", "-----BEGIN CERTIFICATE-----\\nMIIFmzCCA4OgAwIBAgIJAM4UUXYyxPMLcpg16+kuv+CvKSrmQoQn61njCRztkGExMgngbYXHw7mWY=\\n-----END CERTIFICATE-----");

        mockMvc.perform(get("/gms/search")
                        .param("group", "group1")
                        .param("group", "group2")
                        .headers(headers))
                .andExpect(status().isOk())
                .andExpect(content().string("group1\ngroup2\n"));
    }

}