package com.tw.gms.capabilities;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import static com.tw.gms.utils.TestUtils.get;
import static com.tw.gms.utils.TestUtils.set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CapabilitiesServiceTest {

    private static final Answer<?> templateProcessMethod = invocation -> {
        Map<String, String> map = (Map<String, String>) invocation.getArguments()[0];
        StringWriter stringWriter = (StringWriter) invocation.getArguments()[1];
        stringWriter.write(map.toString());
        return null;
    };
    @InjectMocks
    CapabilitiesService capabilitiesService;
    CatalinaProperties catalinaProperties;
    freemarker.template.Configuration ftlConfiguration;
    Template template;

    @BeforeEach
    public void setup() throws NoSuchFieldException, IOException {
        catalinaProperties = new CatalinaProperties();
        String capabilitiesFtlName = "capabilities.ftl";
        ftlConfiguration = mock(freemarker.template.Configuration.class);
        template = mock(Template.class);

        set(capabilitiesService, "catalinaProperties", catalinaProperties);
        set(capabilitiesService, "ftlConfiguration", ftlConfiguration);
        set(capabilitiesService, "capabilitiesFtlName", capabilitiesFtlName);

    }


    @Test
    public void getCapabilitiesFtlContent() throws TemplateException, IOException {
        when(ftlConfiguration.getTemplate(Mockito.anyString())).thenReturn(template);
        doAnswer(templateProcessMethod).when(template).process(any(), any(StringWriter.class));
        assertNotNull(capabilitiesService.getCapabilitiesFtlContent());
    }


    @Test
    public void getCapabilitiesFtlContentCachingTest() throws TemplateException, IOException {
        when(ftlConfiguration.getTemplate(Mockito.anyString())).thenReturn(template);
        doAnswer(templateProcessMethod).when(template).process(any(), any(StringWriter.class));
        String contentForFirstTime = capabilitiesService.getCapabilitiesFtlContent();
        String contentForSecondTime = capabilitiesService.getCapabilitiesFtlContent();
        assertEquals(contentForFirstTime, contentForSecondTime);
    }

    @Test
    public void getCapabilitiesFtlContentCachingTest2() throws TemplateException, IOException, NoSuchFieldException, IllegalAccessException {
        when(ftlConfiguration.getTemplate(Mockito.anyString())).thenReturn(template);
        doAnswer(templateProcessMethod).when(template).process(any(), any(StringWriter.class));
        capabilitiesService.getCapabilitiesFtlContent();
        assertNotNull(get(capabilitiesService, "capabilitiesContent", String.class));
    }
}