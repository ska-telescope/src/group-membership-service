package com.tw.gms.capabilities;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class CapabilitiesConfigurationTest {


    @Test
    public void ftlConfiguration() {
        CapabilitiesConfiguration capabilitiesConfiguration = new CapabilitiesConfiguration();
        assertNotNull(capabilitiesConfiguration.ftlConfiguration("templates"));
    }
}