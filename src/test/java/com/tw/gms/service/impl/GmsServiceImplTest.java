package com.tw.gms.service.impl;

import com.tw.gms.model.Flow;
import com.tw.gms.model.FlowType;
import com.tw.gms.model.GmsException;
import com.tw.gms.service.strategy.WorkFlow;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.tw.gms.utils.TestUtils.setFieldByReflection;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GmsServiceImplTest {

    @InjectMocks
    GmsServiceImpl gmsServiceImpl;


    @Test
    public void isMemberWithTokenFlow() throws GmsException, NoSuchFieldException {
        String group = "group";
        Flow flowType = mock(Flow.class);
        WorkFlow workFlow = mock(WorkFlow.class);
        setFieldByReflection(GmsServiceImpl.class, gmsServiceImpl, "tokenFlow", workFlow);

        when(flowType.getFlowType()).thenReturn(FlowType.TOKEN_FLOW);
        when(workFlow.execute(flowType)).thenReturn(group);

        assertEquals(group, gmsServiceImpl.isMember(flowType));
    }

    @Test
    public void isMemberWithClientCredentialFlow() throws GmsException, NoSuchFieldException {
        String group = "group";
        Flow flowType = mock(Flow.class);
        WorkFlow workFlow = mock(WorkFlow.class);
        setFieldByReflection(GmsServiceImpl.class, gmsServiceImpl, "clientCredentialFlow", workFlow);

        when(flowType.getFlowType()).thenReturn(FlowType.CLIENT_CREDENTIAL_FLOW);
        when(workFlow.execute(flowType)).thenReturn(group);

        assertEquals(group, gmsServiceImpl.isMember(flowType));
    }

    @Test
    public void isMemberWithPasswordGrantFlow() throws GmsException, NoSuchFieldException {
        String group = "group";
        Flow flowType = mock(Flow.class);
        WorkFlow workFlow = mock(WorkFlow.class);
        setFieldByReflection(GmsServiceImpl.class, gmsServiceImpl, "passwordGrantFlow", workFlow);

        when(flowType.getFlowType()).thenReturn(FlowType.PASSWORD_GRANT_FLOW);
        when(workFlow.execute(flowType)).thenReturn(group);

        assertEquals(group, gmsServiceImpl.isMember(flowType));
    }

    @Test
    public void isMemberWithDefaultFlow() throws GmsException, NoSuchFieldException {
        String group = "group";
        Flow flowType = mock(Flow.class);
        WorkFlow workFlow = mock(WorkFlow.class);
        setFieldByReflection(GmsServiceImpl.class, gmsServiceImpl, "defaultFlow", workFlow);

        when(flowType.getFlowType()).thenReturn(FlowType.DEFAULT_FLOW);
        when(workFlow.execute(flowType)).thenReturn(group);

        assertEquals(group, gmsServiceImpl.isMember(flowType));
    }
}