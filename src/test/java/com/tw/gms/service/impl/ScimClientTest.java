package com.tw.gms.service.impl;

import com.tw.gms.connector.ResilientRestClient;
import com.tw.gms.model.ClientTokenResponse;
import com.tw.gms.model.GmsException;
import com.tw.gms.model.ProfileResponse;
import com.tw.gms.model.UserListResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.net.URI;

import static com.tw.gms.utils.TestUtils.setFieldByReflection;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ScimClientTest {

    @InjectMocks
    ScimClient scimClient;

    @Mock
    ResilientRestClient resilientRestClient;

    @Test
    public void fetchUser() throws NoSuchFieldException, GmsException {
        ResponseEntity<ProfileResponse> response = ResponseEntity.ok(ProfileResponse.builder().build());

        setFieldByReflection(ScimClient.class, scimClient, "iamHost", "http://127.0.0.1:8080");
        setFieldByReflection(ScimClient.class, scimClient, "scimProfileApi", "/scim/me");

        when(resilientRestClient.exchange(anyString(), any(URI.class), any(HttpMethod.class), any(HttpEntity.class), eq(ProfileResponse.class))).thenReturn(response);
        assertEquals(response.getBody(), scimClient.fetchUser("token"));
    }


    @Test
    public void fetchClientAccessToken() throws NoSuchFieldException, GmsException {
        String accessToken = "token";
        ClientTokenResponse clientTokenResponse = ClientTokenResponse.builder().accessToken(accessToken).tokenType("Bearer").build();
        ResponseEntity<ClientTokenResponse> response = ResponseEntity.ok(clientTokenResponse);

        setFieldByReflection(ScimClient.class, scimClient, "iamHost", "http://127.0.0.1:8080");
        setFieldByReflection(ScimClient.class, scimClient, "tokenApi", "/token");


        when(resilientRestClient.exchange(anyString(), any(URI.class), any(HttpMethod.class), any(HttpEntity.class), eq(ClientTokenResponse.class))).thenReturn(response);
        assertEquals(accessToken, scimClient.fetchAccessTokenForClientCredentialFlow("clientId", "clientSecret"));
    }

    @Test
    public void fetchClientAccessTokenWithException() throws NoSuchFieldException, GmsException {
        String accessToken = "token";
        ClientTokenResponse clientTokenResponse = ClientTokenResponse.builder().accessToken(accessToken).tokenType("Basic").build();
        ResponseEntity<ClientTokenResponse> response = ResponseEntity.ok(clientTokenResponse);

        setFieldByReflection(ScimClient.class, scimClient, "iamHost", "http://127.0.0.1:8080");
        setFieldByReflection(ScimClient.class, scimClient, "tokenApi", "/token");


        when(resilientRestClient.exchange(anyString(), any(URI.class), any(HttpMethod.class), any(HttpEntity.class), eq(ClientTokenResponse.class))).thenReturn(response);
        assertThrows(GmsException.class, () -> scimClient.fetchAccessTokenForClientCredentialFlow("clientId", "clientSecret"));
    }

    @Test
    public void getUserCount() throws NoSuchFieldException, GmsException {
        String accessToken = "token";
        int userCount = 10;
        UserListResponse userListResponse = UserListResponse.builder().totalResults(userCount).build();
        ResponseEntity<UserListResponse> response = ResponseEntity.ok(userListResponse);

        setFieldByReflection(ScimClient.class, scimClient, "iamHost", "http://127.0.0.1:8080");
        setFieldByReflection(ScimClient.class, scimClient, "getUserCountApi", "/scim/Users?attributes=id&count=1");


        when(resilientRestClient.exchange(anyString(), any(URI.class), any(HttpMethod.class), any(HttpEntity.class), eq(UserListResponse.class))).thenReturn(response);
        assertEquals(userCount, scimClient.getUserCount(accessToken));
    }

    @Test
    public void getUserCountWithException() throws NoSuchFieldException, GmsException {
        String accessToken = "token";
        int userCount = 0;
        UserListResponse userListResponse = UserListResponse.builder().totalResults(userCount).build();
        ResponseEntity<UserListResponse> response = ResponseEntity.ok(userListResponse);

        setFieldByReflection(ScimClient.class, scimClient, "iamHost", "http://127.0.0.1:8080");
        setFieldByReflection(ScimClient.class, scimClient, "getUserCountApi", "/scim/Users?attributes=id&count=1");


        when(resilientRestClient.exchange(anyString(), any(URI.class), any(HttpMethod.class), any(HttpEntity.class), eq(UserListResponse.class)))
                .thenReturn(response);
        assertThrows(GmsException.class, () -> assertEquals(userCount, scimClient.getUserCount(accessToken)));
    }


    @Test
    public void fetchUsersInBatch() throws NoSuchFieldException, GmsException {
        String accessToken = "token";
        UserListResponse userListResponse = UserListResponse.builder().build();
        ResponseEntity<UserListResponse> response = ResponseEntity.ok(userListResponse);

        setFieldByReflection(ScimClient.class, scimClient, "iamHost", "http://127.0.0.1:8080");
        setFieldByReflection(ScimClient.class, scimClient, "fetchUsersInBatchApi", "/scim/Users?attributes=displayName,groups,urn:indigo-dc:scim:schemas:IndigoUser.certificates&startIndex={startIndex}&count={count}");


        when(resilientRestClient.exchange(anyString(), any(URI.class), eq(HttpMethod.GET), any(HttpEntity.class), eq(UserListResponse.class)))
                .thenReturn(response);
        assertEquals(userListResponse, scimClient.fetchUsersInBatch(accessToken, 1, 50));
    }

}