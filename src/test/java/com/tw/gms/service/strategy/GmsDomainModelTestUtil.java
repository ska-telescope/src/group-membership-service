package com.tw.gms.service.strategy;

import com.tw.gms.model.*;

import javax.security.auth.Subject;
import java.util.ArrayList;
import java.util.List;

public class GmsDomainModelTestUtil {

    public static ProfileResponse profileResponse(String groupName) {
        Group group = group(groupName);
        return profileResponse(group);
    }

    public static UserListResponse userListResponse(ProfileResponse... profileResponses) {
        UserListResponse.UserListResponseBuilder userListResponseBuilder = UserListResponse.builder();
        if (0 != profileResponses.length) userListResponseBuilder.resources(new ArrayList<>(List.of(profileResponses)));
        return userListResponseBuilder.build();
    }

    public static GMSDomainModel gmsDomainModel(Subject subject) {
        GMSDomainModel.GMSDomainModelBuilder gmsDomainModelBuilder = GMSDomainModel.builder();
        if (subject != null) {
            gmsDomainModelBuilder.subject(subject);
        }
        return gmsDomainModelBuilder.build();
    }

    public static ProfileResponse profileResponse(Group... groups) {
        ProfileResponse.ProfileResponseBuilder profileResponseBuilder = ProfileResponse.builder();

        if (0 != groups.length) {
            profileResponseBuilder.groups(new ArrayList<>(List.of(groups)));
        }

        return profileResponseBuilder.build();
    }

    public static Group group(String groupName) {
        return Group.builder().display(groupName).build();
    }
}
