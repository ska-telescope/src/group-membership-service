package com.tw.gms.service.strategy;

import com.tw.gms.model.GMSDomainModel;
import com.tw.gms.model.GmsException;
import com.tw.gms.model.ProfileResponse;
import com.tw.gms.service.impl.ScimClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

import javax.security.auth.Subject;

import static com.tw.gms.service.strategy.GmsDomainModelTestUtil.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PasswordGrantFlowTest {

    public static final String EMPTY_STRING = "";
    @InjectMocks
    PasswordGrantFlow passwordGrantFlow;

    @Mock
    Environment environment;

    @Mock
    ScimClient scimClient;

    String clientId = "clientId";
    String clientSecret = "clientSecret";
    String username = "username";
    String password = "password";
    String accessToken = "accessToken";
    Subject subject = new Subject();

    @BeforeEach
    public void setUp() {

    }

    @Test
    public void passwordGrantFlowNotActivated() {
        when(environment.getProperty(eq("password-grant-flow.clientId"), eq(EMPTY_STRING))).thenReturn(EMPTY_STRING);
        when(environment.getProperty(eq("password-grant-flow.clientSecret"), eq(EMPTY_STRING))).thenReturn(EMPTY_STRING);
        when(environment.getProperty(eq("password-grant-flow.username"), eq(EMPTY_STRING))).thenReturn(EMPTY_STRING);
        when(environment.getProperty(eq("password-grant-flow.password"), eq(EMPTY_STRING))).thenReturn(EMPTY_STRING);
        assertThrows(GmsException.class, () -> passwordGrantFlow.execute(Mockito.mock(GMSDomainModel.class)));
    }

    @Test
    public void userFoundWithID() throws GmsException {
        when(environment.getProperty(eq("password-grant-flow.clientId"), eq(EMPTY_STRING))).thenReturn(clientId);
        when(environment.getProperty(eq("password-grant-flow.clientSecret"), eq(EMPTY_STRING))).thenReturn(clientSecret);
        when(environment.getProperty(eq("password-grant-flow.username"), eq(EMPTY_STRING))).thenReturn(username);
        when(environment.getProperty(eq("password-grant-flow.password"), eq(EMPTY_STRING))).thenReturn(password);

        String groupName = "group1";
        GMSDomainModel gmsDomainModel = gmsDomainModel(subject);

        ProfileResponse profileResponse = profileResponse(groupName);

        when(scimClient.fetchAccessTokenForPasswordGrantFlow(eq(username), eq(password), eq(clientId), eq(clientSecret)))
                .thenReturn(accessToken);

        when(scimClient.fetchUserByID(eq(accessToken), eq(subject))).thenReturn(profileResponse);

        assertEquals("group1\n", passwordGrantFlow.execute(gmsDomainModel));

    }

    @Test
    public void noUserFoundWithID() throws GmsException {
        when(environment.getProperty(eq("password-grant-flow.clientId"), eq(EMPTY_STRING))).thenReturn(clientId);
        when(environment.getProperty(eq("password-grant-flow.clientSecret"), eq(EMPTY_STRING))).thenReturn(clientSecret);
        when(environment.getProperty(eq("password-grant-flow.username"), eq(EMPTY_STRING))).thenReturn(username);
        when(environment.getProperty(eq("password-grant-flow.password"), eq(EMPTY_STRING))).thenReturn(password);

        GMSDomainModel gmsDomainModel = gmsDomainModel(subject);

        when(scimClient.fetchAccessTokenForPasswordGrantFlow(eq(username), eq(password), eq(clientId), eq(clientSecret)))
                .thenReturn(accessToken);

        when(scimClient.fetchUserByID(eq(accessToken), eq(subject))).thenReturn(null);

        assertThrows(GmsException.class, () -> passwordGrantFlow.execute(gmsDomainModel));
    }
}