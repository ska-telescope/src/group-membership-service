package com.tw.gms.service.strategy;

import com.tw.gms.model.GMSDomainModel;
import com.tw.gms.model.GmsException;
import com.tw.gms.model.Group;
import com.tw.gms.model.ProfileResponse;
import com.tw.gms.service.impl.ScimClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TokenFlowTest {
    @InjectMocks
    TokenFlow tokenFlow;

    @Mock
    ScimClient scimClient;

    @Test
    public void groups() throws GmsException {
        String token = "Bearer token";
        GMSDomainModel gmsDomainModel = GMSDomainModel.builder()
                .token(token)
                .groups(List.of("group1", "group2"))
                .build();
        ProfileResponse profileResponse = ProfileResponse.builder()
                .groups(List.of(Group.builder().display("group1").build()))
                .build();
        when(scimClient.fetchUser(anyString())).thenReturn(profileResponse);
        assertEquals("group1\n", tokenFlow.execute(gmsDomainModel));
    }

    @Test
    public void groupsWithInvalidToken() {
        String token = "token";
        GMSDomainModel gmsDomainModel = GMSDomainModel.builder()
                .token(token)
                .build();
        assertThrows(GmsException.class, () -> tokenFlow.execute(gmsDomainModel));
    }

    @Test
    public void groupsWithBackendServerError() throws GmsException {
        String token = "Bearer token";
        GMSDomainModel gmsDomainModel = GMSDomainModel.builder()
                .token(token)
                .build();
        when(scimClient.fetchUser(anyString())).thenThrow(mock(GmsException.class));
        assertThrows(GmsException.class, () -> tokenFlow.execute(gmsDomainModel));
    }
}