package com.tw.gms.service.strategy;

import com.tw.gms.model.GMSDomainModel;
import com.tw.gms.model.GmsException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class DefaultFlowTest {
    @InjectMocks
    DefaultFlow defaultFlow;


    @Test
    public void groups() {
        GMSDomainModel gmsDomainModel = GMSDomainModel.builder().build();
        assertThrows(GmsException.class, () -> defaultFlow.execute(gmsDomainModel));
    }
}