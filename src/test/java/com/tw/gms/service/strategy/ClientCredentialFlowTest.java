package com.tw.gms.service.strategy;

import com.tw.gms.model.GMSDomainModel;
import com.tw.gms.model.GmsException;
import com.tw.gms.model.ProfileResponse;
import com.tw.gms.model.UserListResponse;
import com.tw.gms.service.impl.ScimClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

import javax.security.auth.Subject;

import static com.tw.gms.service.strategy.GmsDomainModelTestUtil.*;
import static com.tw.gms.utils.TestUtils.set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ClientCredentialFlowTest {
    @InjectMocks
    ClientCredentialFlow clientCredentialFlow;

    @Mock
    ScimClient scimClient;

    @Mock
    Environment environment;

    String clientId = "clientId";
    String clientSecret = "clientSecret";
    int batchSize = 50;
    int maxBatchCount = 5;
    String token = "client-credential-token";
    final Subject subject = new Subject();

    @BeforeEach
    public void setup() throws NoSuchFieldException {
        set(clientCredentialFlow, "batchSize", batchSize);
        set(clientCredentialFlow, "maxBatchCount", maxBatchCount);

        when(environment.getProperty(eq("scim-read-client.clientId"), eq(""))).thenReturn(clientId);
        when(environment.getProperty(eq("scim-read-client.clientSecret"), eq(""))).thenReturn(clientSecret);
    }

    @Test
    public void clientCredentialFlowNotActivated() {
        assertThrows(GmsException.class, () -> clientCredentialFlow.execute(Mockito.mock(GMSDomainModel.class)));
    }

    @Test
    public void userFoundInSingleUser() throws GmsException {
        int userCount = 1;
        String groupName = "group1";
        GMSDomainModel gmsDomainModel = gmsDomainModel(subject);

        ProfileResponse profileResponse = profileResponse(groupName);
        UserListResponse userListResponse = userListResponse(profileResponse);

        when(scimClient.fetchAccessTokenForClientCredentialFlow(eq(clientId), eq(clientSecret))).thenReturn(token);
        when(scimClient.getUserCount(eq(token))).thenReturn(userCount);
        when(scimClient.fetchUsersInBatch(eq(token), eq(1), eq(batchSize))).thenReturn(userListResponse);
        assertEquals("group1\n", clientCredentialFlow.execute(gmsDomainModel));
    }

    @Test
    public void backendIssueInSingleBatchSingleUser() throws GmsException {
        int userCount = 1;
        when(scimClient.fetchAccessTokenForClientCredentialFlow(eq(clientId), eq(clientSecret))).thenReturn(token);
        when(scimClient.getUserCount(eq(token))).thenReturn(userCount);
        when(scimClient.fetchUsersInBatch(eq(token), eq(1), eq(batchSize))).thenThrow(GmsException.class);
        assertThrows(GmsException.class, () -> clientCredentialFlow.execute(Mockito.mock(GMSDomainModel.class)));
    }
}