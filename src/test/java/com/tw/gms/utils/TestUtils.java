package com.tw.gms.utils;

import java.lang.reflect.Field;

import static org.springframework.util.ReflectionUtils.setField;

public class TestUtils {
    public static void setFieldByReflection(Class<?> classType, Object object, String fieldName, Object fieldValue) throws NoSuchFieldException {
        Field field = classType.getDeclaredField(fieldName);
        field.setAccessible(true);
        setField(field, object, fieldValue);
    }

    public static void set(Object object, String fieldName, Object fieldValue) throws NoSuchFieldException {
        Field field = object.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        setField(field, object, fieldValue);
    }

    public static <T> T get(Object object, String fieldName, Class<T> type) throws NoSuchFieldException, IllegalAccessException {
        Field field = object.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        return type.cast(field.get(object));
    }
}
