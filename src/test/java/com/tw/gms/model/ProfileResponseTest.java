package com.tw.gms.model;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class ProfileResponseTest {

    @Test
    public void getMatchingGroupsAsNewLineSeparated() {
        ProfileResponse profileResponse = ProfileResponse.builder().groups(
                List.of(
                        Group.builder().display("group1").build(),
                        Group.builder().display("group2").build(),
                        Group.builder().display("group3").build()
                )
        ).build();
        assertEquals("group1\n",
                profileResponse.getMatchingGroupsAsNewLineSeparated(
                        List.of("group1"))
        );
    }

    @Test
    public void getMatchingGroupsAsNewLineSeparatedWithNoCommonGroup() {
        ProfileResponse profileResponse = ProfileResponse.builder().groups(
                List.of(
                        Group.builder().display("group1").build(),
                        Group.builder().display("group2").build(),
                        Group.builder().display("group3").build()
                )
        ).build();
        assertEquals("",
                profileResponse.getMatchingGroupsAsNewLineSeparated(
                        List.of("group4"))
        );
    }

    @Test
    public void getMatchingGroupsAsNewLineSeparatedWithNoProfileGroups() {
        ProfileResponse profileResponse = ProfileResponse.builder().build();
        assertEquals("",
                profileResponse.getMatchingGroupsAsNewLineSeparated(
                        List.of("group1"))
        );
    }

    @Test
    public void getMatchingGroupsAsNewLineSeparatedWithNoGivenGroups() {
        ProfileResponse profileResponse = ProfileResponse.builder().groups(
                List.of(
                        Group.builder().display("group1").build(),
                        Group.builder().display("group2").build(),
                        Group.builder().display("group3").build()
                )
        ).build();
        Set<String> groups = new HashSet<>();
        groups.add("group1");
        groups.add("group2");
        groups.add("group3");
        assertEquals(groups,
                Arrays.stream(profileResponse.getMatchingGroupsAsNewLineSeparated(List.of())
                        .split("\n")).collect(Collectors.toSet())
        );
    }


    @Test
    public void hasCertWithDN() {
        ProfileResponse profileResponse = ProfileResponse.builder()
                .indigoUserSchema(IndigoUserSchema.builder()
                        .certificates(
                                List.of(UserCertificate.builder()
                                        .subjectDn("subjectDN")
                                        .pemEncodedCertificate("client-cert")
                                        .build())
                        ).build()
                ).build();
        assertFalse(profileResponse.hasCert("subjectDN", null));
    }

    @Test
    public void hasCertWithClientCert() {
        ProfileResponse profileResponse = ProfileResponse.builder()
                .indigoUserSchema(IndigoUserSchema.builder()
                        .certificates(
                                List.of(UserCertificate.builder()
                                        .subjectDn("subjectDN")
                                        .pemEncodedCertificate("client-cert")
                                        .build())
                        ).build()
                ).build();
        assertFalse(profileResponse.hasCert(null, "client-cert"));
    }

    @Test
    public void hasCertWithDnAndClientCert() {
        ProfileResponse profileResponse = ProfileResponse.builder()
                .indigoUserSchema(IndigoUserSchema.builder()
                        .certificates(
                                List.of(UserCertificate.builder()
                                        .subjectDn("subjectDN")
                                        .pemEncodedCertificate("client-cert")
                                        .build())
                        ).build()
                ).build();
        assertTrue(profileResponse.hasCert("subjectDN", "client-cert"));
    }

    @Test
    public void hasNoCert() {
        ProfileResponse profileResponse = ProfileResponse.builder()
                .build();
        assertFalse(profileResponse.hasCert("subjectD", "client-cert"));
    }

    @Test
    public void hasNoMatchingCert() {
        ProfileResponse profileResponse = ProfileResponse.builder()
                .indigoUserSchema(IndigoUserSchema.builder()
                        .certificates(
                                List.of(UserCertificate.builder()
                                        .subjectDn("subjectDN")
                                        .pemEncodedCertificate("client-cert")
                                        .build())
                        ).build()
                ).build();
        assertFalse(profileResponse.hasCert("subjectDN1", "client-cert1"));
    }
}