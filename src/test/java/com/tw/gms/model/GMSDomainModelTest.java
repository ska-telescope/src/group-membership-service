package com.tw.gms.model;

import org.junit.jupiter.api.Test;

import javax.security.auth.Subject;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GMSDomainModelTest {

    @Test
    public void getTokenFlowType() {
        GMSDomainModel gmsDomainModel = GMSDomainModel.builder()
                .token("Token")
                .build();
        assertEquals(FlowType.TOKEN_FLOW, gmsDomainModel.getFlowType());
    }

    @Test
    public void getClientCredentialFlowType() {
        final Subject subject = new Subject();
        GMSDomainModel gmsDomainModel = GMSDomainModel.builder()
                .subject(subject)
                .build();
        assertEquals(FlowType.CLIENT_CREDENTIAL_FLOW, gmsDomainModel.getFlowType());
    }

    @Test
    public void getPasswordGrantFlowType() {
        GMSDomainModel gmsDomainModel = GMSDomainModel.builder()
                .subject(new Subject())
                .isPasswordGrantFlowActivated(true)
                .build();
        assertEquals(FlowType.PASSWORD_GRANT_FLOW, gmsDomainModel.getFlowType());
    }

    @Test
    public void getDefaultFlowType() {
        GMSDomainModel gmsDomainModel = GMSDomainModel.builder()
                .build();
        assertEquals(FlowType.DEFAULT_FLOW, gmsDomainModel.getFlowType());
    }

}