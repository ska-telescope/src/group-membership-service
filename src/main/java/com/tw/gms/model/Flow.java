package com.tw.gms.model;

public interface Flow {
    FlowType getFlowType();
}
