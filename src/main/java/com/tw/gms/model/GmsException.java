package com.tw.gms.model;

import org.springframework.http.HttpStatus;

public class GmsException extends Exception {
    private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    private final String description;


    public GmsException(String message, HttpStatus httpStatus, String description) {
        super(message);
        this.httpStatus = httpStatus;
        this.description = description;
    }

    public GmsException(String message, String description) {
        super(message);
        this.description = description;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getDescription() {
        return description;
    }

}
