package com.tw.gms.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.Subject;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.util.CollectionUtils.isEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProfileResponse {
    public static final String EMPTY_STRING = "";
    private static final Logger log = LoggerFactory.getLogger(ProfileResponse.class);

    private String id;
    private String displayName;
    private List<Group> groups;
    @JsonProperty("urn:indigo-dc:scim:schemas:IndigoUser")
    private IndigoUserSchema indigoUserSchema;

    private static String getGroupNamesAsNewLineSeparated(Collection<String> collection) {
        return null != collection && !collection.isEmpty() ?
                String.join("\n", collection).concat("\n")
                : EMPTY_STRING;
    }


    private static boolean isNonNull(String string) {
        return null != string && !"null".equals(string) && !string.trim().isEmpty();
    }

    public String getMatchingGroupsAsNewLineSeparated(Collection<String> groups) {
        if (isEmpty(this.getGroups())) {
            log.debug("User belongs to no group");
            return EMPTY_STRING;
        }
        Set<String> groupNamesAsSet = groupNamesAsSet();
        log.info("User group list {}", this.getGroups());
        if (isEmpty(groups)) {
            return getGroupNamesAsNewLineSeparated(groupNamesAsSet);
        }
        List<String> filteredGroups = groups.stream()
                .filter(groupNamesAsSet::contains)
                .distinct()
                .collect(Collectors.toList());
        return getGroupNamesAsNewLineSeparated(filteredGroups);
    }

    private Set<String> groupNamesAsSet() {
        if (!isEmpty(this.getGroups()))
            return this.getGroups().stream()
                    .map(Group::getDisplay)
                    .collect(Collectors.toSet());
        return new HashSet<>();
    }

    public boolean hasCert(String subjectDn, String clientCert) {
        // TODO review the implementation
        if (null != getIndigoUserSchema() && !isEmpty(getIndigoUserSchema().getCertificates())) {
            String finalClientCert = isNonNull(clientCert) ? clientCert.replace("\t", "\n") : null;
            return this
                    .getIndigoUserSchema()
                    .getCertificates()
                    .stream()
                    .anyMatch(userCertificate -> userCertificate.getSubjectDn().equals(subjectDn) &&
                            userCertificate.getPemEncodedCertificate().equals(finalClientCert)
                    );
        }
        return false;
    }
}
