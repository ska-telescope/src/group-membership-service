package com.tw.gms.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserListResponse {
    private int totalResults;
    private int itemsPerPage;
    private int startIndex;
    @JsonProperty("Resources")
    private List<ProfileResponse> resources = new ArrayList<>();
}
