package com.tw.gms.model;

public enum FlowType {
    TOKEN_FLOW("TOKEN_FLOW"), CLIENT_CREDENTIAL_FLOW("CLIENT_CREDENTIAL_FLOW"),
    DEFAULT_FLOW("DEFAULT_FLOW"),PASSWORD_GRANT_FLOW("PASSWORD_GRANT_FLOW");
    private final String value;

    FlowType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
