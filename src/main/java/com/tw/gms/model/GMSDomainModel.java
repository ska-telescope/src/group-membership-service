package com.tw.gms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.security.auth.Subject;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GMSDomainModel implements Flow {
    String token;
    Subject subject;
    List<String> groups;

    boolean isPasswordGrantFlowActivated;

    @Override
    public FlowType getFlowType() {
        if (token != null)
            return FlowType.TOKEN_FLOW;
        if (subject != null) {
            if (isPasswordGrantFlowActivated) return FlowType.PASSWORD_GRANT_FLOW;
            else return FlowType.CLIENT_CREDENTIAL_FLOW;
        }
        return FlowType.DEFAULT_FLOW;
    }
}
