package com.tw.gms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Batch {
    private List<Integer> startingIndices;
    private int batchCount;
    private int batchSize;
}
