package com.tw.gms.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserCertificate {
    private boolean primary;
    private String subjectDn;
    private String issuerDn;
    private String pemEncodedCertificate;
    private String display;
    private String created;
    private String lastModified;
    private boolean hasProxyCertificate;
}
