package com.tw.gms.connector;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tw.gms.model.GmsException;
import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Service
public class ResilientRestClient {

    private static final Gson gson = new GsonBuilder().disableHtmlEscaping().create();
    private static final String TRUE = "true";

    @Value("${logging.show.rest-call-response}")
    String showRestCallResponse;

    @Value("${logging.show.rest-call-headers}")
    String showRestCallHeaders;

    @Value("${logging.show.rest-call-uri}")
    String showRestCallUri;

    @Value("${logging.show.rest-call-request}")
    String showRestCallRequest;


    @Autowired
    RestTemplate restTemplate;

    @Autowired
    CircuitBreakerFactory circuitBreakerFactory;

    Logger log = LoggerFactory.getLogger(ResilientRestClient.class);

    public <T> ResponseEntity<T> exchange(String hystrixKey, URI url, HttpMethod method, HttpEntity<?> httpEntity, Class<T> type, T defaultResponse) {
        log.debug("hystrixKey is {}", hystrixKey);
        CircuitBreaker circuitBreaker = getCircuitBreaker(hystrixKey);
        Map<String, String> contextMap = getContextMap();
        return circuitBreaker.run(
                () -> getResponseEntity(url, method, httpEntity, type, contextMap),
                throwable -> getDefaultResponseEntity(defaultResponse, contextMap)
        );
    }

    public <T> ResponseEntity<T> exchange(String hystrixKey, URI url, HttpMethod method, HttpEntity<?> httpEntity, Class<T> type) throws GmsException {
        log.debug("hystrix key is {}", hystrixKey);
        CircuitBreaker circuitBreaker = getCircuitBreaker(hystrixKey);
        Map<String, String> contextMap = getContextMap();
        try {
            return circuitBreaker.run(
                    () -> getResponseEntity(url, method, httpEntity, type, contextMap),
                    this::throwRuntimeException
            );
        } catch (RuntimeException exception) {
            return buildException(exception);
        }
    }

    private <T> ResponseEntity<T> getResponseEntity(URI url, HttpMethod method, HttpEntity<?> httpEntity, Class<T> type, Map<String, String> contextMap) {
        MDC.setContextMap(contextMap);
        if (TRUE.equals(showRestCallUri)) log.info("calling to {} ", url);
        if (TRUE.equals(showRestCallHeaders)) log.debug("headers are {}", httpEntity.getHeaders());
        if (TRUE.equals(showRestCallRequest)) log.debug("request are {}", httpEntity.getBody());
        ResponseEntity<T> responseEntity = restTemplate.exchange(url, method, httpEntity, type);
        if (TRUE.equals(showRestCallResponse)) log.info("response is {}", gson.toJson(responseEntity.getBody()));
        return responseEntity;
    }

    private <T> ResponseEntity<T> getDefaultResponseEntity(T defaultResponse, Map<String, String> contextMap) {
        MDC.setContextMap(contextMap);
        log.info("returning the default response {}", defaultResponse);
        return ResponseEntity.ok(defaultResponse);
    }

    private <T> ResponseEntity<T> throwRuntimeException(Throwable throwable) {
        log.error("throwing exception from rest-template {}", throwable.getMessage());
        throwable.printStackTrace();
        throw new RuntimeException(throwable);
    }


    private Map<String, String> getContextMap() {
        if (null != MDC.getCopyOfContextMap()) return MDC.getCopyOfContextMap();
        if (null != ThreadContext.getContext()) return ThreadContext.getContext();
        return new HashMap<>();
    }

    private <T> T buildException(RuntimeException exception) throws GmsException {
        Throwable cause = exception.getCause();
        if (cause instanceof HttpClientErrorException httpClientErrorException) {
            throw new GmsException("Unauthorized Access",
                    httpClientErrorException.getStatusCode(),
                    httpClientErrorException.getResponseBodyAsString());
        }
        throw new GmsException("Internal server error",
                null != cause ? cause.getMessage() : exception.getMessage());
    }

    private CircuitBreaker getCircuitBreaker(String hystrixKey) {
        CircuitBreaker circuitBreaker = circuitBreakerFactory.create(hystrixKey);
        if (null == circuitBreaker) {
            circuitBreaker = circuitBreakerFactory.create("default");
        }
        return circuitBreaker;
    }
}
