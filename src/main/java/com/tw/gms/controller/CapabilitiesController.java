package com.tw.gms.controller;

import com.tw.gms.capabilities.CapabilitiesService;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class CapabilitiesController {

    @Autowired
    CapabilitiesService capabilitiesService;

    @GetMapping(value = "/gms/capabilities", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<?> getCapabilities() throws TemplateException, IOException {
        return ResponseEntity.ok(capabilitiesService.getCapabilitiesFtlContent());
    }
}
