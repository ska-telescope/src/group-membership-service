package com.tw.gms.controller.advisor;

import com.tw.gms.model.ErrorResponse;
import com.tw.gms.model.GmsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GmsControllerAdvisor extends ResponseEntityExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GmsControllerAdvisor.class);

    @ExceptionHandler(GmsException.class)
    public ResponseEntity<?> handleRestCallException(
            GmsException ex, HttpServletRequest request) {
        log.error("Error in controller advisor is {}", ex.getMessage());
        return ResponseEntity
                .status(ex.getHttpStatus())
                .body(new ErrorResponse(ex.getMessage(), ex.getDescription()));
    }
}
