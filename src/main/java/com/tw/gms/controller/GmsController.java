package com.tw.gms.controller;

import ca.nrc.cadc.auth.AuthMethod;
import ca.nrc.cadc.auth.AuthenticationUtil;
import ca.nrc.cadc.auth.NotAuthenticatedException;
import com.tw.gms.model.GMSDomainModel;
import com.tw.gms.model.GmsException;
import com.tw.gms.service.GmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class GmsController {
    @Autowired
    GmsService gmsService;

    @Value("${password-grant-flow.active}")
    boolean passwordGrantFlow;

    @GetMapping(value = "/gms/search", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> isAMember
            (@RequestParam(name = "group", required = false) List<String> groups,
             final HttpServletRequest request) throws GmsException {

        final Subject currentSubject = AuthenticationUtil.getSubject(request);
        final AuthMethod authMethod = AuthenticationUtil.getAuthMethod(currentSubject);
        if (authMethod == AuthMethod.ANON) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("User is not authenticated to use GMS.");
        }

        GMSDomainModel gmsDomainModel = GMSDomainModel.builder()
                .subject(currentSubject)
                .groups(groups)
                .isPasswordGrantFlowActivated(passwordGrantFlow)
                .build();

        try {
            return ResponseEntity.ok(gmsService.isMember(gmsDomainModel));
        } catch (NotAuthenticatedException notAuthenticatedException) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(notAuthenticatedException.getMessage());
        }
    }
}