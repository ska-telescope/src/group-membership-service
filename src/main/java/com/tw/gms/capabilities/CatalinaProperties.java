package com.tw.gms.capabilities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "tomcat.connector")
@Configuration
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CatalinaProperties {
    private String secure = "true";
    private String scheme = "https";
    private String proxyName = "gms";
    private String proxyPort = "443";
}
