package com.tw.gms.capabilities;

import com.tw.gms.GroupMembershipServiceApplication;
import freemarker.template.TemplateExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CapabilitiesConfiguration {

    @Bean("ftlConfiguration")
    public freemarker.template.Configuration ftlConfiguration(@Value("${ftl.templates.folderName:/templates}") String templateFolderName) {
        freemarker.template.Configuration configuration = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_32);
        configuration.setDefaultEncoding("UTF-8");
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        configuration.setLogTemplateExceptions(false);
        configuration.setClassForTemplateLoading(GroupMembershipServiceApplication.class, templateFolderName);
        return configuration;
    }
}
