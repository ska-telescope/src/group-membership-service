package com.tw.gms.capabilities;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@Service
public class CapabilitiesService {

    @Autowired
    @Qualifier("ftlConfiguration")
    freemarker.template.Configuration ftlConfiguration;
    @Autowired
    CatalinaProperties catalinaProperties;
    private String capabilitiesContent = null;
    @Value("${ftl.templates.name.capabilities:capabilities.ftl}")
    private String capabilitiesFtlName;

    public String getCapabilitiesFtlContent() throws TemplateException, IOException {
        if (null != capabilitiesContent) return capabilitiesContent;
        Template template = ftlConfiguration.getTemplate(capabilitiesFtlName);
        StringWriter stringWriter = new StringWriter();
        template.process(convertToDataModel(catalinaProperties), stringWriter);
        capabilitiesContent = stringWriter.toString();
        return capabilitiesContent;
    }


    private Map<String, String> convertToDataModel(CatalinaProperties catalinaProperties) {
        Map<String, String> data = new HashMap<>();
        data.put("proxyName", catalinaProperties.getProxyName());
        data.put("proxyPort", catalinaProperties.getProxyPort());
        data.put("secure", catalinaProperties.getSecure());
        data.put("scheme", catalinaProperties.getScheme());
        return data;
    }
}
