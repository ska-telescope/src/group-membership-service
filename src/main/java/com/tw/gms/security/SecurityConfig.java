package com.tw.gms.security;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.springframework.util.CollectionUtils.isEmpty;

@Configuration
public class SecurityConfig {
    private static final String ALL = "all";
    private static final Set<String> ALLOWED_HEADER_NAMES = new HashSet<>();
    Logger log = LoggerFactory.getLogger(SecurityConfig.class);
    @Value("#{'${firewall.policy.allowedHeaderNames}'.split(',')}")
    private List<String> allowedHeaderNames;

    @Bean
    @ConditionalOnProperty(prefix = "application", name = "https", havingValue = "true")
    public SecurityFilterChain filterChainForHttps(HttpSecurity http) throws Exception {
        log.debug("Adding https filter for every request");
        return http
                .requiresChannel(channel -> channel.anyRequest().requiresSecure())
                .authorizeRequests(authorize -> authorize.anyRequest().permitAll())
                .build();
    }

    @Bean
    @ConditionalOnProperty(prefix = "application", name = "https", havingValue = "false")
    public SecurityFilterChain filterChainForHttp(HttpSecurity http) throws Exception {
        log.debug("removing default security filter for http");
        return http
                .authorizeRequests(authorize -> authorize.anyRequest().permitAll())
                .build();
    }

    @Bean
    public HttpFirewall getHttpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();

        firewall.setAllowUrlEncodedSlash(true);
        firewall.setAllowBackSlash(true);
        firewall.setAllowUrlEncodedPercent(true);
        firewall.setAllowUrlEncodedPeriod(true);
        firewall.setAllowSemicolon(true);
        buildAllowedHeadersNameSet(allowedHeaderNames);
        firewall.setAllowedHeaderNames(headerName -> {
            if (ALLOWED_HEADER_NAMES.contains(ALL) || ALLOWED_HEADER_NAMES.contains(headerName)) return true;
            log.error("{} is not allowed in headers", headerName);
            return false;
        });
        // Allow all header values
        firewall.setAllowedHeaderValues(header -> true);
        return firewall;
    }

    private void buildAllowedHeadersNameSet(List<String> allowedHeaderNames) {
        log.debug("allowed header names are {}", allowedHeaderNames);
        if (isEmpty(ALLOWED_HEADER_NAMES) && !isEmpty(allowedHeaderNames)) {
            ALLOWED_HEADER_NAMES.addAll(allowedHeaderNames);
        }
        log.debug("allowed headers-name set is {}", ALLOWED_HEADER_NAMES);
    }
}