package com.tw.gms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


/**
 * Main application.
 * <p></p>
 * It was necessary to explicitly disable the DataSourceAutoConfiguration here as Spring attempted to connect to a
 * non-existent database otherwise.
 * jenkinsd 2024.03.21
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class GroupMembershipServiceApplication {
    private static final Logger log = LoggerFactory.getLogger(GroupMembershipServiceApplication.class);

    public static void main(String[] args) {
        log.info("Runtime arguments are ");
        for (String arg : args) {
            log.info("{}", arg);
        }
        SpringApplication.run(GroupMembershipServiceApplication.class, args);
    }

}
