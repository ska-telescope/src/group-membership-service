package com.tw.gms.service;

import com.tw.gms.model.Flow;
import com.tw.gms.model.GmsException;

public interface GmsService {
    String isMember(Flow flowType) throws GmsException;
}
