package com.tw.gms.service.strategy;

import com.tw.gms.model.Flow;
import com.tw.gms.model.GMSDomainModel;
import com.tw.gms.model.GmsException;
import com.tw.gms.model.ProfileResponse;
import com.tw.gms.service.impl.ScimClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TokenFlow implements WorkFlow {

    private static final Logger log = LoggerFactory.getLogger(TokenFlow.class);
    private static final String BEARER_TOKEN_START = "Bearer ";

    @Autowired
    ScimClient scimClient;

    @Override
    public String execute(Flow flowType) throws GmsException {
        log.info("token flow for GMS group search is executing");

        GMSDomainModel gmsDomainModel = (GMSDomainModel) flowType;
        String token = gmsDomainModel.getToken();
        List<String> groups = gmsDomainModel.getGroups();

        if (!isBearerToken(token))
            throw new GmsException("Excepted bearer token", HttpStatus.UNAUTHORIZED, "Excepted bearer not found");
        token = token.substring(BEARER_TOKEN_START.length());
        log.debug("Extracted bearer token {}", token);
        log.info("Incoming group list is {}", groups);
        ProfileResponse profileResponse = scimClient.fetchUser(token);
        return profileResponse.getMatchingGroupsAsNewLineSeparated(groups);
    }

    private boolean isBearerToken(String token) {
        return null != token
                && token.length() > BEARER_TOKEN_START.length()
                && token.substring(0, BEARER_TOKEN_START.length()).equalsIgnoreCase(BEARER_TOKEN_START);
    }
}
