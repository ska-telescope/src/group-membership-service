package com.tw.gms.service.strategy;

import com.tw.gms.model.Flow;
import com.tw.gms.model.GMSDomainModel;
import com.tw.gms.model.GmsException;
import com.tw.gms.model.ProfileResponse;
import com.tw.gms.service.impl.ScimClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.tw.gms.service.impl.StringUtils.isEmpty;

@Service
public class PasswordGrantFlow implements WorkFlow {

    private static final Logger log = LoggerFactory.getLogger(PasswordGrantFlow.class);
    public static final String EMPTY_STRING = "";

    @Autowired
    Environment environment;

    @Autowired
    ScimClient scimClient;

    @Override
    public String execute(Flow flow) throws GmsException {
        log.info("Password grant flow for GMS group search is executing");
        GMSDomainModel gmsDomainModel = (GMSDomainModel) flow;
        List<String> groups = gmsDomainModel.getGroups();

        String adminName = environment.getProperty("password-grant-flow.username", EMPTY_STRING);
        String adminPassword = environment.getProperty("password-grant-flow.password", EMPTY_STRING);
        String clientId = environment.getProperty("password-grant-flow.clientId", EMPTY_STRING);
        String clientSecret = environment.getProperty("password-grant-flow.clientSecret", EMPTY_STRING);

        if (isEmpty(adminName, adminPassword, clientId, clientSecret))
            throw new GmsException("Configuration missing for password grant flow",
                    HttpStatus.FORBIDDEN,
                    "Password Grant flow is not active for this GMS instance,Contact System Administrator");

        String accessToken = scimClient.fetchAccessTokenForPasswordGrantFlow(adminName, adminPassword, clientId,
                                                                             clientSecret);
        ProfileResponse profileResponse = scimClient.fetchUserByID(accessToken, gmsDomainModel.getSubject());

        if (profileResponse == null) {
            throw new GmsException("Unauthorized Access",
                                   HttpStatus.UNAUTHORIZED, "Current Client Certificate is not linked with any user");
        }
        return profileResponse.getMatchingGroupsAsNewLineSeparated(groups);
    }
}
