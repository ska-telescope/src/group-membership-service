package com.tw.gms.service.strategy;

import com.tw.gms.model.*;
import com.tw.gms.service.impl.ScimClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.security.auth.Subject;
import java.util.List;


import static com.tw.gms.service.impl.StringUtils.isEmpty;

@Service
public class ClientCredentialFlow implements WorkFlow {

    private static final Logger log = LoggerFactory.getLogger(ClientCredentialFlow.class);

    @Autowired
    ScimClient scimClient;

    @Autowired
    Environment environment;


    public static final String EMPTY_STRING = "";


    @Override
    public String execute(Flow flowType) throws GmsException {
        log.info("Client credential flow for GMS group search is executing");

        String clientId = environment.getProperty("scim-read-client.clientId", EMPTY_STRING);
        String clientSecret = environment.getProperty("scim-read-client.clientSecret", EMPTY_STRING);

        if (isEmpty(clientId, clientSecret))
            throw new GmsException("Client certificate is currently not accepted",
                    HttpStatus.FORBIDDEN, "Client credential flow is not active for this GMS instance,Contact System Administrator");

        GMSDomainModel gmsDomainModel = (GMSDomainModel) flowType;
        List<String> groups = gmsDomainModel.getGroups();
        Subject subject = gmsDomainModel.getSubject();

        String accessToken = scimClient.fetchAccessTokenForClientCredentialFlow(clientId, clientSecret);

        ProfileResponse profileResponse = scimClient.fetchUserByID(accessToken, subject);
        return profileResponse.getMatchingGroupsAsNewLineSeparated(groups);
    }
}
