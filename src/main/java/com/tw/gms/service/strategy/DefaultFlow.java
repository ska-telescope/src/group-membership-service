package com.tw.gms.service.strategy;

import com.tw.gms.model.Flow;
import com.tw.gms.model.GMSDomainModel;
import com.tw.gms.model.GmsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class DefaultFlow implements WorkFlow {

    private static final Logger log = LoggerFactory.getLogger(DefaultFlow.class);

    @Override
    public String execute(Flow flowType) throws GmsException {
        GMSDomainModel gmsDomainModel = (GMSDomainModel) flowType;
        log.info("Default flow for GMS group search is executing");
        log.info("token {}", gmsDomainModel.getToken());
        log.info("subject {}", gmsDomainModel.getSubject());
        throw new GmsException(
                "Bad Request",
                HttpStatus.UNAUTHORIZED,
                "Either token or certificate is required to identify user"
        );
    }
}
