package com.tw.gms.service.strategy;

import com.tw.gms.model.Flow;
import com.tw.gms.model.GmsException;

public interface WorkFlow {
    String execute(Flow flow) throws GmsException;
}
