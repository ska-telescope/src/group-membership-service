package com.tw.gms.service.impl;

public class StringUtils {

    public static final String EMPTY_STRING = "";

    private StringUtils() {
    }

    public static boolean isNotEmpty(String... strings) {
        for (String string : strings)
            if (null == string || EMPTY_STRING.equals(string.trim()))
                return false;
        return true;
    }

    public static boolean isEmpty(String... strings) {
        return !isNotEmpty(strings);
    }
}
