package com.tw.gms.service.impl;

import com.tw.gms.model.Flow;
import com.tw.gms.model.GmsException;
import com.tw.gms.service.GmsService;
import com.tw.gms.service.strategy.WorkFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class GmsServiceImpl implements GmsService {

    @Autowired
    @Qualifier("clientCredentialFlow")
    WorkFlow clientCredentialFlow;

    @Autowired
    @Qualifier("passwordGrantFlow")
    WorkFlow passwordGrantFlow;

    @Autowired
    @Qualifier("tokenFlow")
    WorkFlow tokenFlow;

    @Autowired
    @Qualifier("defaultFlow")
    WorkFlow defaultFlow;

    @Override
    public String isMember(Flow flow) throws GmsException {
        return switch (flow.getFlowType()) {
            case TOKEN_FLOW -> tokenFlow.execute(flow);
            case CLIENT_CREDENTIAL_FLOW -> clientCredentialFlow.execute(flow);
            case PASSWORD_GRANT_FLOW -> passwordGrantFlow.execute(flow);
            default -> defaultFlow.execute(flow);
        };
    }

}
