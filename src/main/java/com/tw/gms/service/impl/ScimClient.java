package com.tw.gms.service.impl;

import ca.nrc.cadc.auth.NotAuthenticatedException;
import ca.nrc.cadc.auth.OpenIdPrincipal;
import com.tw.gms.connector.ResilientRestClient;
import com.tw.gms.model.ClientTokenResponse;
import com.tw.gms.model.GmsException;
import com.tw.gms.model.ProfileResponse;
import com.tw.gms.model.UserListResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import javax.security.auth.Subject;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


@Service
public class ScimClient {

    private static final Logger log = LoggerFactory.getLogger(ScimClient.class);
    public static final String BEARER = "Bearer";

    @Value("${backend.host.iam}")
    String iamHost;

    @Value("${backend.path.currentUser}")
    String scimProfileApi;

    @Value("${backend.path.token}")
    String tokenApi;

    @Value("${backend.path.fetchUsersInBatch}")
    String fetchUsersInBatchApi;

    @Value("${backend.path.getUserCount}")
    String getUserCountApi;

    @Value("${backend.path.fetchUserByID}")
    String fetchUserByID;

    @Autowired
    ResilientRestClient resilientRestClient;

    public ProfileResponse fetchUser(String token) throws GmsException {
        URI uri = UriComponentsBuilder
                .fromHttpUrl(iamHost + scimProfileApi)
                .build().toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.ALL));
        headers.setBearerAuth(token);
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);
        return resilientRestClient
                .exchange("iam-profile", uri, HttpMethod.GET, httpEntity, ProfileResponse.class)
                .getBody();
    }

    public String fetchAccessTokenForClientCredentialFlow(String clientId, String clientSecret) throws GmsException {
        MultiValueMap<String, String> request = new LinkedMultiValueMap<>();
        request.add("grant_type", "client_credentials");
        request.add("client_id", clientId);
        request.add("client_secret", clientSecret);
        return getBearerToken(request);
    }


    public String fetchAccessTokenForPasswordGrantFlow(String username, String password,
                                                       String clientId, String clientSecret) throws GmsException {
        MultiValueMap<String, String> request = new LinkedMultiValueMap<>();
        request.add("grant_type", "password");
        request.add("username", username);
        request.add("password", password);
        request.add("client_id", clientId);
        request.add("client_secret", clientSecret);
        return getBearerToken(request);
    }

    public int getUserCount(String token) throws GmsException {
        URI uri = UriComponentsBuilder
                .fromHttpUrl(iamHost + getUserCountApi).build().toUri();
        log.info("uri for calling users {}", uri);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.ALL));
        headers.setBearerAuth(token);
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);
        UserListResponse userListResponse = resilientRestClient
                .exchange("iam-profile", uri, HttpMethod.GET, httpEntity, UserListResponse.class)
                .getBody();
        if (null == userListResponse || 0 == userListResponse.getTotalResults()) {
            throw new GmsException("Unable to fetch user count / User count is 0", HttpStatus.INTERNAL_SERVER_ERROR,
                                   "Unable to fetch ");
        }
        return userListResponse.getTotalResults();
    }

    public UserListResponse fetchUsersInBatch(String token, int startingIndex, int count) throws GmsException {
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("startIndex", Integer.toString(startingIndex));
        uriVariables.put("count", Integer.toString(count));
        URI uri = UriComponentsBuilder
                .fromHttpUrl(iamHost + fetchUsersInBatchApi)
                .buildAndExpand(uriVariables).toUri();
        log.info("uri for calling users {}", uri);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.ALL));
        headers.setBearerAuth(token);
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);
        return resilientRestClient
                .exchange("iam-profile", uri, HttpMethod.GET, httpEntity, UserListResponse.class)
                .getBody();
    }

    public ProfileResponse fetchUserByID(String token, Subject subject) throws GmsException {
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("user_id", ScimClient.getUserID(subject));
        URI uri = UriComponentsBuilder
                .fromHttpUrl(iamHost + fetchUserByID)
                .buildAndExpand(uriVariables).toUri();
        log.info("uri for calling user by id {}", uri);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.ALL));
        headers.setBearerAuth(token);
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);
        return resilientRestClient
                .exchange("iam-profile", uri, HttpMethod.GET, httpEntity, ProfileResponse.class)
                .getBody();
    }

    private static String getUserID(Subject subject) {
        final Iterator<OpenIdPrincipal> openIDPrincipalIterator =
                subject.getPrincipals(OpenIdPrincipal.class).iterator();
        if (!openIDPrincipalIterator.hasNext()) {
            throw new NotAuthenticatedException("No Open ID credentials found in Subject. " + subject);
        } else {
            return openIDPrincipalIterator.next().getName();
        }
    }

    private String getBearerToken(MultiValueMap<String, String> request) throws GmsException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccept(List.of(MediaType.ALL));
        HttpEntity<?> httpEntity = new HttpEntity<>(request, headers);
        URI uri = UriComponentsBuilder
                .fromHttpUrl(iamHost + tokenApi)
                .build().toUri();
        ClientTokenResponse clientTokenResponse = resilientRestClient
                .exchange("iam-profile", uri, HttpMethod.POST, httpEntity, ClientTokenResponse.class)
                .getBody();
        if (isBearerToken(clientTokenResponse)) {
            return clientTokenResponse.getAccessToken();
        }
        throw new GmsException("Null Token/Token type mismatch",
                               HttpStatus.INTERNAL_SERVER_ERROR,
                               "Unable to retrieve Access token for client credential flow");
    }

    private boolean isBearerToken(ClientTokenResponse clientTokenResponse) {
        return null != clientTokenResponse && null != clientTokenResponse.getAccessToken()
               && BEARER.equalsIgnoreCase(clientTokenResponse.getTokenType());
    }
}
