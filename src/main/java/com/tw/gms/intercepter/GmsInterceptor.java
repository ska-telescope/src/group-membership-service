package com.tw.gms.intercepter;

import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class GmsInterceptor implements HandlerInterceptor {

    public static final String TRUE = "true";
    private static final Logger log = LoggerFactory.getLogger(GmsInterceptor.class);
    @Value("${logging.show.incoming-headers}")
    String showIncomingHeaders;
    @Value("${logging.show.incoming-headers-name}")
    String showIncomingHeadersName;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        Map<String, String> context = new HashMap<>();
        String uuid = UUID.randomUUID().toString();
        context.put("uuid", uuid);
        context.put("method", request.getMethod());
        context.put("uri", request.getRequestURI());
        context.put("startTime", String.valueOf(System.currentTimeMillis()));
        ThreadContext.putAll(context);
        MDC.setContextMap(context);

        if (TRUE.equalsIgnoreCase(showIncomingHeaders)) {
            log.debug("Incoming headers are ");
            for (String headerName : Collections.list(request.getHeaderNames())) {
                log.debug("{} : {}", headerName, request.getHeader(headerName));
            }
        } else if (TRUE.equalsIgnoreCase(showIncomingHeadersName)) {
            log.debug("Incoming headers are {}", Collections.list(request.getHeaderNames()));
        }
        log.debug("transaction started for uuid {}", uuid);
        return true;
    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
                                @Nullable Exception ex) throws Exception {
        Map<String, String> context = MDC.getCopyOfContextMap();
        if (!CollectionUtils.isEmpty(context)) {
            long responseTime = System.currentTimeMillis() - Long.parseLong(context.get("startTime"));
            String uuid = context.get("uuid");
            log.debug("transaction completed for uuid {}", uuid);
            log.info("ResponseCode={}|ResponseTime={}ms", response.getStatus(), responseTime);
            ThreadContext.clearMap();
            MDC.clear();
        }
    }
}
