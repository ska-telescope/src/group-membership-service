<?xml version="1.0" encoding="UTF-8"?>
<vosi:capabilities
	xmlns:vosi="http://www.ivoa.net/xml/VOSICapabilities/v1.0"
	xmlns:vs="http://www.ivoa.net/xml/VODataService/v1.1"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<capability standardID="ivo://ivoa.net/std/VOSI#capabilities">
		<interface xsi:type="vs:ParamHTTP" role="std">
			<accessURL use="full">${scheme}://${proxyName}:${proxyPort}/gms/capabilities</accessURL>
		</interface>
	</capability>
	<capability standardID="ivo://ivoa.net/std/GMS#groups-0.1">
		<interface xsi:type="vs:ParamHTTP" role="std" version="0.1">
			<accessURL use="base">${scheme}://${proxyName}:${proxyPort}/gms/search</accessURL>
			<securityMethod standardID="ivo://ivoa.net/sso#cookie"/>
			<securityMethod standardID="ivo://ivoa.net/sso#tls-with-certificate"/>
			<securityMethod standardID="ivo://ivoa.net/sso#token"/>
			<securityMethod standardID="vos://cadc.nrc.ca~vospace/CADC/std/Auth#token-1.0"/>
		</interface>
	</capability>
	<capability standardID="ivo://ivoa.net/std/GMS#search-0.1">
		<interface xsi:type="vs:ParamHTTP" role="std" version="0.1">
			<accessURL use="base">${scheme}://${proxyName}:${proxyPort}/gms/search</accessURL>
			<securityMethod standardID="ivo://ivoa.net/sso#cookie"/>
			<securityMethod standardID="ivo://ivoa.net/sso#tls-with-certificate"/>
			<securityMethod standardID="ivo://ivoa.net/sso#token"/>
			<securityMethod standardID="vos://cadc.nrc.ca~vospace/CADC/std/Auth#token-1.0"/>
		</interface>
	</capability>
	<capability standardID="ivo://ivoa.net/std/GMS#search-1.0">
		<interface xsi:type="vs:ParamHTTP" role="std" version="1.0">
			<accessURL use="base">${scheme}://${proxyName}:${proxyPort}/gms/search</accessURL>
			<securityMethod standardID="ivo://ivoa.net/sso#cookie"/>
			<securityMethod standardID="ivo://ivoa.net/sso#tls-with-certificate"/>
			<securityMethod standardID="ivo://ivoa.net/sso#token"/>
		</interface>
	</capability>
</vosi:capabilities>