# =========== BUILD STAGE =====================
FROM maven:3.9 AS build
WORKDIR /workspace/app

COPY pom.xml pom.xml

# build maven .m2 cache as layer for reuse
# Disabled due to strange error downloading incorrect dependencies
# 2024.04.02 jenkinsd
#RUN mvn dependency:go-offline -B

# build application as fat executable JAR
COPY src src
RUN mvn clean package -DskipTests -U

# explode fat executable JAR for COPY in RUN stage
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

# =========== RUN STAGE =====================
FROM eclipse-temurin:19-jdk
VOLUME /tmp

ARG DEPENDENCY=/workspace/app/target/dependency
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app

RUN mkdir /root/config

EXPOSE 8080
ENTRYPOINT java ${JVM_OPTS} -cp app:app/lib/* com.tw.gms.GroupMembershipServiceApplication ${GMS_JAVA_OPTS}
