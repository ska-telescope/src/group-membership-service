
GMS_IMAGE_NAME=gms/gms:latest
GMS_APP_NAME=gms-app
GMS_ENV_PATH="${PWD}"/env/env-http
GMS_NETWORK=gms-net


#cd nginx && sh gms-nginx-build.sh && cd ../
sh gms-build.sh


# shellcheck disable=SC2046
if [ -z $(docker network ls --filter name=^${GMS_NETWORK}$ --format="{{ .Name }}") ] ; then
     docker network create ${GMS_NETWORK}
fi

docker rm -f ${GMS_APP_NAME}

docker run -d \
    --net=${GMS_NETWORK} \
    -p 8085:8080 \
    --name ${GMS_APP_NAME} \
    --env-file="${GMS_ENV_PATH}" \
    ${GMS_IMAGE_NAME}

#sh ./nginx/gms-nginx-run.sh