# SKA GMS service (0.1.0)

## 2024.10.18 (0.1.0)
- RED-135
- Fix for using proper principal to issue authenticated requests to the Identity Provider
- Fix to properly lookup the OpenID Connect Discovery document (`.well-known` document) to obtain the `userinfo` endpoint